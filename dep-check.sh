#!/bin/bash
DEPENDENCY_CHECK_VERSION=$(curl -fsSLI -o /dev/null -w%{url_effective} https://github.com/jeremylong/DependencyCheck/releases/latest | awk -F"/" '{print $NF}')
curl -sSfL https://github.com/jeremylong/DependencyCheck/releases/download/${DEPENDENCY_CHECK_VERSION}/dependency-check-${DEPENDENCY_CHECK_VERSION:1}-release.zip -o dependency-check.zip
unzip dependency-check.zip -d $HOME && rm dependency-check.zip
chmod 755 $HOME/dependency-check/bin/dependency-check.sh
ln -s $HOME/dependency-check/bin/dependency-check.sh /usr/local/bin/dependency-check